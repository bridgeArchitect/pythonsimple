from django.shortcuts import render

def main_page(request):

    # find POST request
    if request.method == "POST":
        # receive numbers
        first_number = request.POST.get("first_number")
        second_number = request.POST.get("second_number")
        # convert to numbers
        first_number = int(first_number)
        second_number = int(second_number)
        # add numbers
        res = first_number + second_number
        # save result in row
        answer = "Answer: " + str(res)
        # prepare result for Django
        data = {"answer": answer}
        # return modified page
        return render(request, "main_page.html", context=data)

    # return default page
    return render(request, "main_page.html")
